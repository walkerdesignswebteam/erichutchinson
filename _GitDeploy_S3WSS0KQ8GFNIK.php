<?php
require '/_walkersystems/scripts/GitDeploy.class.php';
new GitDeploy(array(
	'bare_repo_path'=>'/home/erichutchinson/mirror.git',
	'web_root_path'=>'/home/erichutchinson/public_html',
	'deploy_branch'=>'master',
	//If you want more debug info in the log. Add this.
	//'debug_mode'=>true,
));