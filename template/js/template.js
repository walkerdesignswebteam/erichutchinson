function highlightActiveLinks(a_menuPrefixArray,parentIdString){
	//e.g. highlightActiveLinks(['mm_','mobmm_'],'[[*id]],[[CommonTools? &cmd=`parentIds` &pageID=`[[*id]]`]]');
	var a_ids=parentIdString.split(',');
	var i=0;
	var j=0;
	for(i=0;i<a_menuPrefixArray.length;i++){
		for(j=0;j<a_ids.length;j++){
			var listEl = $('#'+a_menuPrefixArray[i]+a_ids[j]);
			if(listEl.length>0){
				listEl.addClass('active');
			}
		}
	}
}

// Makes ALL vimeo + youtube videos responsive without anything fancy.
(function ( window, document, undefined ) {

    /*
    * Grab all iframes on the page or return
    */
    var iframes = document.getElementsByTagName( 'iframe' );

    /*
    * Loop through the iframes array
    */
    for ( var i = 0; i < iframes.length; i++ ) {

        var iframe = iframes[i],

        /*
        * RegExp, extend this if you need more players
        */
        players = /www.youtube.com|player.vimeo.com/;

        /*
        * If the RegExp pattern exists within the current iframe
        */
        if ( iframe.src.search( players ) > 0 ) {

            /*
            * Calculate the video ratio based on the iframe's w/h dimensions
            */
            var videoRatio        = ( iframe.height / iframe.width ) * 100;
            
            /*
            * Replace the iframe's dimensions and position
            * the iframe absolute, this is the trick to emulate
            * the video ratio
            */
            iframe.style.position = 'absolute';
            iframe.style.top      = '0';
            iframe.style.left     = '0';
            iframe.width          = '100%';
            iframe.height         = '100%';
            
            /*
            * Wrap the iframe in a new <div> which uses a
            * dynamically fetched padding-top property based
            * on the video's w/h dimensions
            */
            var wrap              = document.createElement( 'div' );
            wrap.className        = 'fluid-vids';
            wrap.style.width      = '100%';
            wrap.style.position   = 'relative';
            wrap.style.paddingTop = videoRatio + '%';
            
            /*
            * Add the iframe inside our newly created <div>
            */
            var iframeParent      = iframe.parentNode;
            iframeParent.insertBefore( wrap, iframe );
            wrap.appendChild( iframe );

        }

    }

})( window, document );

jQuery(document).ready(function(){
    var $_date = $('#date');
    if($_date.length > 0){
        $_date.datepicker({dateFormat: 'dd/mm/yy'});
    }
    
    var $_date_required = $('#date_required');
    if($_date_required.length > 0){
        $_date_required.datepicker({dateFormat: 'dd/mm/yy'});
    }

    $(".placeholderField").focus(function(){
    	if($(this).val()==$(this).attr('title')){
        	$(this).val('');
        }
    });
    $(".placeholderField").blur(function(){
    	if($(this).val()==''){
       		$(this).val($(this).attr('title'));
       	}
    });
    $(".placeholderField").blur();


    $("#header").sticky({topSpacing:0});


     
    /*********************************************************************************************/
    /*External links to _blank target (allows valid XHTML strict whilst opening external windows)*/
    /*********************************************************************************************/
    $('a[rel="external"]').click(function(){
	    this.target = "_blank";
    });
	
    /*************************/
    /* Window resize scripts */
    /*************************/
    function resizeSets(){
    $('.homeSlider .block').height('auto');
         $('.homeSlider .block').matchHeight();

        $('.quickLink > div').height('auto');
        $('.quickLink > div').matchHeight();

        $('.articleListItem > a > div').height('auto');
        $('.articleListItem > a > div').matchHeight();

    }
    $(window).resize(function(e){resizeSets();});
    resizeSets();









	/*********************/
	/* SUB MENU BUTTON */
	/*********************/

	$('.subButton').click(function(e) {
			$('.subButton').next().slideUp('normal');
			$('.subButton').removeClass('active');
		if($(this).next().is(':hidden') == true) {
			$(this).addClass('active');
			$(this).next().slideDown('normal');
		}
		});
	$('#submenu > ul').hide();

    $('.accordionButton').click(function(e) {
        $('.accordionContent').slideUp('normal');
		$('.accordionButton').removeClass('active');
        if ($(this).parent().find('.accordionContent').is(':hidden') == true) {
            $(this).next().slideDown('normal');
			$(this).addClass('active');
        }
    });
    $('.accordionContent').hide();
    $('.accordionContent.active').slideDown('normal');

	



		$('.accordion_button').click(function(){
		var allLists = $('.accordion_content');
		var thisList = $(this).parent().find('.accordion_content');
		
			var jumpEl = $(this).parent();
			//console.log('id: '+jumpEl);
						
		if(thisList.css('display')=='none'){
			allLists.slideUp('normal');
			allLists.parent().removeClass('active');
			thisList.slideToggle('normal', function() {
			  });
			thisList.parent().addClass('active');
			
}else{
			thisList.slideToggle('slow');
			thisList.parent().removeClass('active');
		}
	});
	$('.accordion_content').hide();
	




	  //mobile main menu
    var mobNav = $("#mobile-nav");
    if(mobNav.length>0){
        mobNav.mmenu({
            slidingSubmenus: false,
            offCanvas: {
                position:'right'
            }
        });
        $("#mobile-menu-butt").click(function() {
           mobNav.trigger("open.mm");
        });
        mobNav.find('.active').addClass('mm-opened');
    }
	
});


$(document).ready(function() {
    $(window).bind('scroll',function(e){
    	parallaxScrollBanner();
    });
});

function parallaxScrollBanner(){
  var scrolledY = $(window).scrollTop();
  $('#banner').css('top',( (scrolledY)/2 ) + 'px');
  $('#banner .container').css('opacity',( 1 - (scrolledY)*.0014) );
  
}


