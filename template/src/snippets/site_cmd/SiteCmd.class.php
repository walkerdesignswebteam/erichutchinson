<?php

//may want to require this is using formitbuilder forms (perhaps load as needed rather than in class head.
//require_once MODX_CORE_PATH.'components/formitbuilder/model/formitbuilder/FormItBuilder.class.php';
//Note - You can of course create a global reference to this object in order to run directly via PHP.
require_once 'Core.class.php';

class SiteCmd extends Core {

    function __construct(&$modx) {
        $this->modx = &$modx;
        parent::__construct($this->modx);
    }

    //[[!site_cmd? &json_options=`{"cmd":"output_mycontent"}`]]
    function requestOutput($json_options, $extraArg = NULL) {
        $s_ret = '';
        $a_commands = json_decode($json_options);
        switch ($a_commands->cmd) {
            case 'output_mycontent':
                $s_ret .= $this->output_mycontent();
                break;
            case 'outputForm_contactForm':
                $s_ret .= $this->outputForm_contactForm();
                break;
            case 'outputForm_congratulatoryMessage':
                $s_ret .= $this->outputForm_congratulatoryMessage();
                break;
            case 'outputForm_registerGroup':
                $s_ret .= $this->outputForm_registerGroup();
                break;
            case 'outputForm_requestFlag':
                $s_ret .= $this->outputForm_requestFlag();
                break;
            case 'outputForm_requestSupportLetter':
                $s_ret .= $this->outputForm_requestSupportLetter();
                break;
            case 'output_searchResults': $s_ret .= $this->output_searchResults();
                break;
            case 'output_SEOSummary': 
                $s_ret .= $this->output_SEOSummary();
                break;
            default: return '--- ' . $a_commands->cmd . ' is not a valid command ---';
        }
        return $s_ret;
    }

    public function output_searchResults() {
        $s_searchVal = $this->getVal('search');
        $s_ret = '<div class="siteSearchForm">[[!SimpleSearchForm]]</div>';

        if (empty($s_searchVal) === false && strlen($s_searchVal) > 2) {

            $s_ret.= '<hr />

                    [[!SimpleSearch?
                        &toPlaceholder=`sisea.results`
                        &perPage=`500`
                    ]]

                    [[!If?
                        &subject=`[[+sisea.total]]`
                        &operator=`EQ`
                        &operand=`0`
                        &then=``
                        &else=`<h2>Found Pages ([[+sisea.total]])</h2>[[+sisea.results]]`
                    ]]

                    [[!If?
                        &subject=`[[+sisea.total]][[+sisea.places.total]]`
                        &operator=`EQ`
                        &operand=`00`
                        &then=`<h2>No results found</h2>`
                    ]]
            ';
        }
        return $s_ret;
    }

    function output_mycontent() {
        return 'blah';
    }

    function outputForm_contactForm() {
        require_once $this->modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/jsonformbuilder/model/jsonformbuilder/JsonFormBuilder.class.php';

        //CREATE FORM ELEMENTS
        $o_fe_name = new JsonFormBuilder_elementText('name_first', 'First Name');
        $o_fe_surname = new JsonFormBuilder_elementText('name_last', 'Surname');
        $o_fe_email = new JsonFormBuilder_elementText('email_address', 'Email Address');
        $o_fe_phone = new JsonFormBuilder_elementText('phone', 'Phone');
        $o_fe_state = new JsonFormBuilder_elementText('state', 'State');
        $o_fe_postcode = new JsonFormBuilder_elementText('postcode', 'Postcode');
        $o_fe_city = new JsonFormBuilder_elementText('city', 'City');
        $o_fe_address = new JsonFormBuilder_elementText('address', 'Address');
        
		$o_fe_interestedIn    = new JsonFormBuilder_elementCheckboxGroup('interested_in','I am interested in',array(
			array('title'=>'Helping Eric at election time','checked'=>false),
			array('title'=>'Hearing from Eric on local issues','checked'=>false),
			array('title'=>'Working with Eric on local issues','checked'=>false),

		 ));

		$a_contactMethodOptions = array(
			'Phone'=>'Phone',
			'Email'=>'Email',
			'Any'=>'Any'
		);
		$o_fe_contactMethod = new JsonFormBuilder_elementRadioGroup('contactMethod','Preferred method of contact',$a_contactMethodOptions);


        $o_fe_notes = new JsonFormBuilder_elementTextArea('comments', 'Comments', 5, 30);

        $o_fe_buttSubmit = new JsonFormBuilder_elementButton('submit', 'Send message', 'submit');

        //SET VALIDATION RULES
        $a_formRules = array();
        //Set required fields
        $a_formFields_required = array($o_fe_notes, $o_fe_surname, $o_fe_name, $o_fe_email, $o_fe_phone,  $o_fe_address,  $o_fe_city, $o_fe_state, $o_fe_postcode);
        foreach ($a_formFields_required as $field) {
            $a_formRules[] = new FormRule(FormRuleType::required, $field);
              $field->setExtraElementAttribs(array('placeholder' => $field->getLabel()));
      }




        //Make email field require a valid email address
        $a_formRules[] = new FormRule(FormRuleType::email, $o_fe_email, NULL, 'Please provide a valid email address');

        //CREATE FORM AND SETUP
        $o_form = new JsonFormBuilder($this->modx, 'contactForm');
        $o_form->setRedirectDocument(3);
        $o_form->addRules($a_formRules);
        $o_form->setEmailToAddress($this->modx->getOption('emailsender'));
        $o_form->setEmailFromAddress($o_form->postVal('email_address'));
        $o_form->setEmailFromName($o_form->postVal('name_full'));
        $o_form->setEmailSubject('Contact Form Submission - From: ' . $o_form->postVal('name_full'));
        $o_form->setEmailHeadHtml('<p>This is a response sent by ' . $o_form->postVal('name_full') . ' using the contact us form:</p>');
        $o_form->setJqueryValidation(true);
        $o_form->setPlaceholderJavascript('JsonFormBuilder_myForm');

        //ADD ELEMENTS TO THE FORM IN PREFERRED ORDER
        $o_form->addElements(
                array(
                    '<div class="formSegWrap Half hideLabel">', $o_fe_name, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_surname, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_phone, '</div>',
                    '<div class="formSegWrap hideLabel Full">',  $o_fe_address, '</div>',					
                    '<div class="formSegWrap hideLabel Half">',  $o_fe_city, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_state, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_postcode, '</div>',
                    '<div class="formSegWrap Full">',  $o_fe_contactMethod, '</div>',					
                    '<div class="formSegWrap Full hideLabel">',  $o_fe_notes, '</div>',
                    '<div class="formSegWrap Full">',  $o_fe_interestedIn, '</div>',					
                    '<div class="formSegWrap Full">',  $o_fe_buttSubmit, '</div>',
                )
        );

        // The form HTML will now be available via
        return $o_form->output();
        //This can be returned in a snippet or passed to any other script to handle in any way.
    }
    
    function outputForm_congratulatoryMessage() {
        require_once $this->modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/jsonformbuilder/model/jsonformbuilder/JsonFormBuilder.class.php';

        //CREATE FORM ELEMENTS
        $o_fe_name = new JsonFormBuilder_elementText('name_first', 'First Name');
        $o_fe_surname = new JsonFormBuilder_elementText('name_last', 'Surname');
        $o_fe_email = new JsonFormBuilder_elementText('email_address', 'Email Address');
        $o_fe_email2 = new JsonFormBuilder_elementText('email_address2', 'Confirm Email Address');
        $o_fe_email2->showInEmail(false);
        $o_fe_phone = new JsonFormBuilder_elementText('phone', 'Your Phone');
        
        $o_fe_recipients = new JsonFormBuilder_elementText('name_first', 'Name of recipients/s');
        $o_fe_date = new JsonFormBuilder_elementText('date', 'Recipent/s Date of marriage/birth');
        
        $o_fe_address = new JsonFormBuilder_elementText('address', 'Recipient/s Address');
        $o_fe_city = new JsonFormBuilder_elementText('city', 'City');
        $o_fe_state = new JsonFormBuilder_elementText('state', 'State');
        $o_fe_postcode = new JsonFormBuilder_elementText('postcode', 'Postcode');
        
        $a_yesNo = array('yes'=>'Yes','no'=>'No');
        $o_fe_sendMessage = new JsonFormBuilder_elementRadioGroup('sendMessage','Send message to this address?',$a_yesNo);
        
        $o_fe_dateRequired = new JsonFormBuilder_elementText('date_required', 'Date message is required (*up to one month prior to event)');
        
		$o_fe_messageType    = new JsonFormBuilder_elementSelect('message_type','Type of message',array(
			''=>'Please select...',
			'wedding'=>'Wedding Anniversary',
			'birthday'=>'Birthday',
		 ));

        $o_fe_additional_info      = new JsonFormBuilder_elementFile('additional_info', 'Upload additional information');
                
        $o_fe_buttSubmit = new JsonFormBuilder_elementButton('submit', 'Submit request', 'submit');

        //SET VALIDATION RULES
        $a_formRules = array();
        //Set required fields
        $a_formFields_required = array(  $o_fe_name, $o_fe_surname,$o_fe_email,$o_fe_email2, $o_fe_phone, $o_fe_recipients, $o_fe_date, $o_fe_address,  $o_fe_city, $o_fe_state, $o_fe_postcode,$o_fe_sendMessage,$o_fe_dateRequired,$o_fe_messageType);
        foreach ($a_formFields_required as $field) {
            $a_formRules[] = new FormRule(FormRuleType::required, $field);
              $field->setExtraElementAttribs(array('placeholder' => $field->getLabel()));
      }


        //Make email field require a valid email address
        $a_formRules[] = new FormRule(FormRuleType::email, $o_fe_email, NULL, 'Please provide a valid email address');
        $a_formRules[] = new FormRule(FormRuleType::fieldMatch,$o_fe_email2, $o_fe_email, 'Email addresses do not match');
        $a_formRules[] = new FormRule(FormRuleType::date, $o_fe_date, 'dd/mm/yyyy');
        $a_formRules[] = new FormRule(FormRuleType::date, $o_fe_dateRequired, 'dd/mm/yyyy');
        
        //CREATE FORM AND SETUP
        $o_form = new JsonFormBuilder($this->modx, 'groupRegistration');
        $o_form->setRedirectDocument(43);
        $o_form->addRules($a_formRules);
        $o_form->setEmailToAddress($this->modx->getOption('emailsender'));
        $o_form->setEmailFromAddress($o_form->postVal('email_address'));
        $o_form->setEmailFromName($o_form->postVal('name_full'));
        $o_form->setEmailSubject('Community Group Registration - From: ' . $o_form->postVal('name_full'));
        $o_form->setEmailHeadHtml('<p>This is a response sent by ' . $o_form->postVal('name_full') . ' using the Community Group Registration form:</p>');
        $o_form->setJqueryValidation(true);
        //$o_form->setPlaceholderJavascript('JsonFormBuilder_myForm');

        //ADD ELEMENTS TO THE FORM IN PREFERRED ORDER
        $o_form->addElements(
                array(
                    '<div class="formSegWrap Half hideLabel">', $o_fe_name, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_surname, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email2, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_phone, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_recipients, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_date, '</div>',
                    '<div class="formSegWrap hideLabel Full">',  $o_fe_address, '</div>',					
                    '<div class="formSegWrap hideLabel Half">',  $o_fe_city, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_state, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_postcode, '</div>',
                    '<div class="formSegWrap Full">',  $o_fe_sendMessage, '</div>',					
                    '<div class="formSegWrap Full hideLabel">', $o_fe_dateRequired, '</div>',
                    '<div class="formSegWrap Full">', $o_fe_messageType, '</div>',					
                    '<div class="formSegWrap Full">', $o_fe_additional_info, '</div>',					
                    '<div class="formSegWrap Full">', $o_fe_buttSubmit, '</div>',
                )
        );

        // The form HTML will now be available via
        return $o_form->output();
        //This can be returned in a snippet or passed to any other script to handle in any way.
    }
    function outputForm_registerGroup() {
        require_once $this->modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/jsonformbuilder/model/jsonformbuilder/JsonFormBuilder.class.php';

        //CREATE FORM ELEMENTS
        $o_fe_organisation = new JsonFormBuilder_elementText('organisation', 'Organisation');
        $o_fe_name = new JsonFormBuilder_elementText('name_first', 'First Name');
        $o_fe_surname = new JsonFormBuilder_elementText('name_last', 'Surname');
        $o_fe_position = new JsonFormBuilder_elementText('position', 'Position within the organisation');
        $o_fe_email = new JsonFormBuilder_elementText('email_address', 'Email Address');
        $o_fe_email2 = new JsonFormBuilder_elementText('email_address2', 'Confirm Email Address');
        $o_fe_email2->showInEmail(false);
        $o_fe_phone = new JsonFormBuilder_elementText('phone', 'Your Phone');
        
        $o_fe_address = new JsonFormBuilder_elementText('address', 'Address');
        $o_fe_city = new JsonFormBuilder_elementText('city', 'City');
        $o_fe_state = new JsonFormBuilder_elementText('state', 'State');
        $o_fe_postcode = new JsonFormBuilder_elementText('postcode', 'Postcode');
        
        $o_fe_comments         = new JsonFormBuilder_elementTextArea('purpose','Purpose of Organisation',5,30,'');
        
        $o_fe_buttSubmit = new JsonFormBuilder_elementButton('submit', 'Submit registration', 'submit');

        //SET VALIDATION RULES
        $a_formRules = array();
        //Set required fields
        $a_formFields_required = array( $o_fe_organisation,$o_fe_position, $o_fe_name, $o_fe_surname,$o_fe_email,$o_fe_email2, $o_fe_phone, $o_fe_address,  $o_fe_city, $o_fe_state, $o_fe_postcode);
        foreach ($a_formFields_required as $field) {
            $a_formRules[] = new FormRule(FormRuleType::required, $field);
              $field->setExtraElementAttribs(array('placeholder' => $field->getLabel()));
      }

        //Make email field require a valid email address
        $a_formRules[] = new FormRule(FormRuleType::email, $o_fe_email, NULL, 'Please provide a valid email address');
        $a_formRules[] = new FormRule(FormRuleType::fieldMatch,$o_fe_email2, $o_fe_email, 'Email addresses do not match');
        
        //CREATE FORM AND SETUP
        $o_form = new JsonFormBuilder($this->modx, 'groupRegistration');
        $o_form->setRedirectDocument(42);
        $o_form->addRules($a_formRules);
        $o_form->setEmailToAddress($this->modx->getOption('emailsender'));
        $o_form->setEmailFromAddress($o_form->postVal('email_address'));
        $o_form->setEmailFromName($o_form->postVal('name_full'));
        $o_form->setEmailSubject('Group Registration - From: ' . $o_form->postVal('name_full'));
        $o_form->setEmailHeadHtml('<p>This is a response sent by ' . $o_form->postVal('name_full') . ' using the Group Registration form:</p>');
        $o_form->setJqueryValidation(true);
        //$o_form->setPlaceholderJavascript('JsonFormBuilder_myForm');

        //ADD ELEMENTS TO THE FORM IN PREFERRED ORDER
        $o_form->addElements(
                array(
                    '<div class="formSegWrap Half hideLabel">', $o_fe_organisation, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_name, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_surname, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_position, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email2, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_phone, '</div>',
                    '<div class="formSegWrap hideLabel Full">',  $o_fe_address, '</div>',					
                    '<div class="formSegWrap hideLabel Half">',  $o_fe_city, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_state, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_postcode, '</div>',
                    '<div class="formSegWrap Full">', $o_fe_comments, '</div>',					
                    '<div class="formSegWrap Full">', $o_fe_buttSubmit, '</div>',
                )
        );

        // The form HTML will now be available via
        return $o_form->output();
        //This can be returned in a snippet or passed to any other script to handle in any way.
    }
    function outputForm_requestFlag() {
        require_once $this->modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/jsonformbuilder/model/jsonformbuilder/JsonFormBuilder.class.php';

        //CREATE FORM ELEMENTS
        $o_fe_name = new JsonFormBuilder_elementText('name_first', 'First Name');
        $o_fe_surname = new JsonFormBuilder_elementText('name_last', 'Surname');
        $o_fe_organisation = new JsonFormBuilder_elementText('organisation', 'Organisation');
        $o_fe_position = new JsonFormBuilder_elementText('position', 'Position in Organisation');
        $o_fe_email = new JsonFormBuilder_elementText('email_address', 'Email Address');
        $o_fe_email2 = new JsonFormBuilder_elementText('email_address2', 'Confirm Email Address');
        $o_fe_email2->showInEmail(false);
        $o_fe_phone = new JsonFormBuilder_elementText('phone', 'Your Phone');
        
        $o_fe_address = new JsonFormBuilder_elementText('address', 'Recipient/s Address');
        $o_fe_city = new JsonFormBuilder_elementText('city', 'City');
        $o_fe_state = new JsonFormBuilder_elementText('state', 'State');
        $o_fe_postcode = new JsonFormBuilder_elementText('postcode', 'Postcode');
        
        $a_yesNo = array('yes'=>'Yes','no'=>'No');
        $o_fe_present = new JsonFormBuilder_elementRadioGroup('present','Would you like me to present the flag to your organisation?',$a_yesNo);
        
        
		$o_fe_type    = new JsonFormBuilder_elementSelect('type','Type of Flag',array(
			'australian'=>'Australian Flag',
			'torres_strait'=>'Torres Strait Islander Flag',
			'aboriginal'=>'Aboriginal Flag',
		 ));

        $o_fe_buttSubmit = new JsonFormBuilder_elementButton('submit', 'Submit request', 'submit');

        //SET VALIDATION RULES
        $a_formRules = array();
        //Set required fields
        $a_formFields_required = array(  $o_fe_name, $o_fe_surname,$o_fe_organisation, $o_fe_position,$o_fe_email,$o_fe_email2, $o_fe_phone, $o_fe_address,  $o_fe_city, $o_fe_state, $o_fe_postcode,$o_fe_present,$o_fe_type);
        foreach ($a_formFields_required as $field) {
            $a_formRules[] = new FormRule(FormRuleType::required, $field);
              $field->setExtraElementAttribs(array('placeholder' => $field->getLabel()));
      }


        //Make email field require a valid email address
        $a_formRules[] = new FormRule(FormRuleType::email, $o_fe_email, NULL, 'Please provide a valid email address');
        $a_formRules[] = new FormRule(FormRuleType::fieldMatch,$o_fe_email2, $o_fe_email, 'Email addresses do not match');
        
        //CREATE FORM AND SETUP
        $o_form = new JsonFormBuilder($this->modx, 'groupRegistration');
        $o_form->setRedirectDocument(44);
        $o_form->addRules($a_formRules);
        $o_form->setEmailToAddress($this->modx->getOption('emailsender'));
        $o_form->setEmailFromAddress($o_form->postVal('email_address'));
        $o_form->setEmailFromName($o_form->postVal('name_full'));
        $o_form->setEmailSubject('Flag Request - From: ' . $o_form->postVal('name_full'));
        $o_form->setEmailHeadHtml('<p>This is a response sent by ' . $o_form->postVal('name_full') . ' using the Flag Request form:</p>');
        $o_form->setJqueryValidation(true);
        //$o_form->setPlaceholderJavascript('JsonFormBuilder_myForm');

        //ADD ELEMENTS TO THE FORM IN PREFERRED ORDER
        $o_form->addElements(
                array(
                    '<div class="formSegWrap Half hideLabel">', $o_fe_name, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_surname, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_organisation, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_position, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email2, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_phone, '</div>',
                    '<div class="formSegWrap hideLabel Full">',  $o_fe_address, '</div>',					
                    '<div class="formSegWrap hideLabel Half">',  $o_fe_city, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_state, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_postcode, '</div>',
                    '<div class="formSegWrap Full">',  $o_fe_present, '</div>',					
                    '<div class="formSegWrap Full hideLabel">', $o_fe_type, '</div>',
                    '<div class="formSegWrap Full">', $o_fe_buttSubmit, '</div>',
                )
        );

        // The form HTML will now be available via
        return $o_form->output();
        //This can be returned in a snippet or passed to any other script to handle in any way.
    }
    function outputForm_requestSupportLetter() {
        require_once $this->modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/jsonformbuilder/model/jsonformbuilder/JsonFormBuilder.class.php';

        //CREATE FORM ELEMENTS
        $o_fe_organisation = new JsonFormBuilder_elementText('organisation', 'Organisation');
        $o_fe_name = new JsonFormBuilder_elementText('name_first', 'Contact First Name');
        $o_fe_surname = new JsonFormBuilder_elementText('name_last', 'Contact Last Name');
        $o_fe_position = new JsonFormBuilder_elementText('position', 'Position');
        $o_fe_email = new JsonFormBuilder_elementText('email_address', 'Email Address');
        $o_fe_email2 = new JsonFormBuilder_elementText('email_address2', 'Confirm Email Address');
        $o_fe_email2->showInEmail(false);
        $o_fe_phone = new JsonFormBuilder_elementText('phone', 'Your Phone');
        
        $o_fe_address = new JsonFormBuilder_elementText('address', 'Recipient/s Address');
        $o_fe_city = new JsonFormBuilder_elementText('city', 'City');
        $o_fe_state = new JsonFormBuilder_elementText('state', 'State');
        $o_fe_postcode = new JsonFormBuilder_elementText('postcode', 'Postcode');
        
        $o_fe_purpose      = new JsonFormBuilder_elementTextArea('purpose', 'Purpose of Organisation',5,30,'');
        
        $o_fe_reason      = new JsonFormBuilder_elementTextArea('reason_needed', 'Reason Letter Needed',5,30,'');
        $o_fe_benefits      = new JsonFormBuilder_elementTextArea('benefits', 'Benefits to the Organisation',5,30,'');
        $o_fe_community      = new JsonFormBuilder_elementTextArea('community', 'Benefits to the Community',5,30,'');
        $o_fe_date = new JsonFormBuilder_elementText('date', 'Date Letter Required');
        $o_fe_additionalInfo      = new JsonFormBuilder_elementTextArea('additional_info', 'Additional Infofmation',5,30,'');
                
        $o_fe_buttSubmit = new JsonFormBuilder_elementButton('submit', 'Submit request', 'submit');

        //SET VALIDATION RULES
        $a_formRules = array();
        //Set required fields
        $a_formFields_required = array( $o_fe_organisation, $o_fe_name, $o_fe_surname,$o_fe_position,$o_fe_email,$o_fe_email2, $o_fe_phone, $o_fe_address,  $o_fe_city, $o_fe_state, $o_fe_postcode,$o_fe_purpose,$o_fe_reason, $o_fe_benefits, $o_fe_community, $o_fe_date, $o_fe_additionalInfo);
        foreach ($a_formFields_required as $field) {
            $a_formRules[] = new FormRule(FormRuleType::required, $field);
              $field->setExtraElementAttribs(array('placeholder' => $field->getLabel()));
      }


        //Make email field require a valid email address
        $a_formRules[] = new FormRule(FormRuleType::email, $o_fe_email, NULL, 'Please provide a valid email address');
        $a_formRules[] = new FormRule(FormRuleType::fieldMatch,$o_fe_email2, $o_fe_email, 'Email addresses do not match');
        $a_formRules[] = new FormRule(FormRuleType::date, $o_fe_date, 'dd/mm/yyyy');
        
        //CREATE FORM AND SETUP
        $o_form = new JsonFormBuilder($this->modx, 'groupRegistration');
        $o_form->setRedirectDocument(45);
        $o_form->addRules($a_formRules);
        $o_form->setEmailToAddress($this->modx->getOption('emailsender'));
        $o_form->setEmailFromAddress($o_form->postVal('email_address'));
        $o_form->setEmailFromName($o_form->postVal('name_full'));
        $o_form->setEmailSubject('Letter of Support Request - From: ' . $o_form->postVal('name_full'));
        $o_form->setEmailHeadHtml('<p>This is a response sent by ' . $o_form->postVal('name_full') . ' using the Letter of Support Request form:</p>');
        $o_form->setJqueryValidation(true);
        //$o_form->setPlaceholderJavascript('JsonFormBuilder_myForm');

        //ADD ELEMENTS TO THE FORM IN PREFERRED ORDER
        $o_form->addElements(
                array(
                    '<div class="formSegWrap Half hideLabel">', $o_fe_organisation, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_name, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_surname, '</div>',
                    '<div class="formSegWrap Half hideLabel">', $o_fe_position, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_email2, '</div>',
                    '<div class="formSegWrap Half hideLabel">',  $o_fe_phone, '</div>',
                    '<div class="formSegWrap hideLabel Full">',  $o_fe_address, '</div>',					
                    '<div class="formSegWrap hideLabel Half">',  $o_fe_city, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_state, '</div>',
                    '<div class="formSegWrap hideLabel Quarter">', $o_fe_postcode, '</div>',
                    '<div class="formSegWrap Full">', $o_fe_purpose, '</div>',					
                    '<div class="formSegWrap Full">', $o_fe_reason, '</div>',
                    '<div class="formSegWrap Full">', $o_fe_benefits, '</div>',					
                    '<div class="formSegWrap Full">', $o_fe_community, '</div>',
                    '<div class="formSegWrap Half">', $o_fe_date, '</div>',					
                    '<div class="formSegWrap Full">', $o_fe_additionalInfo, '</div>',
                    '<div class="formSegWrap Full">', $o_fe_buttSubmit, '</div>',
                )
        );

        // The form HTML will now be available via
        return $o_form->output();
        //This can be returned in a snippet or passed to any other script to handle in any way.
    }

    private function output_SEOSummary(){
        require_once MODX_BASE_PATH.'template/src/snippets/SEOSummary.class.php';
        $o_seoSummary = new SEOSummary($this->modx);
        $o_seoSummary->outputSummary();
    }
    
}
