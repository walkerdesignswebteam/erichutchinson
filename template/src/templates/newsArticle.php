[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body class="showMenu">

	<div>
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	<div>
	</div>

	<section id="banner" class="articleImage container-fluid noPadding" style="background-image:url([[pthumb? &input=`[[*articleImage]]` &options=`q=75` &useResizer=`0`]]);">
		<div class="imageParallax" style="background-image:url([[*articleImage:phpthumbof=`w=1800&h=850&q=70&zc=c&far=c&aoe=1`]]);"></div>
	</section>

	</div>

	<section class="content">

		<div class="container">
			[[!CommonTools? &cmd=`loadChunk` &name=`breadcrumbs`]]
			<div><hr /></div>

			<div class="row">
				<main id="content" role="main" class="col-xs-12 col-sm-7">
					<h1 itemprop="name">[[*pagetitle]]</h1>
					<p class="post-info">
						<span class="left">Posted on <time itemprop="dateCreated" datetime="[[*publishedon:strtotime:date=`%Y-%m-%d`]]">[[*publishedon:strtotime:date=`%b %d, %Y`]]</time> by <a href="[[~[[*parent]]]]author/[[*publishedby:userinfo=`username`]]">[[*publishedby:userinfo=`username`]]</a></span>
						[[*articlestags:notempty=`<span class="tags left">&nbsp;| Tags: [[+article_tags]]</span>`]]
					</p>				
					<div itemprop="text">[[*content]]</div>                
				</main>
				<aside class="col-xs-12 col-sm-offset-1 col-sm-4">
					[[!CommonTools? &cmd=`loadChunk` &name=`articleSidebar`]] 
				</aside>
			</div><!--/.row -->

		</div><!--/container -->
		
	</section><!--/content -->

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]] 
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>