[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body class="showMenu">
	<div>
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
	</div>

	<section class="content blogListing grey">
		<div class="container">
			<div class="row">
				<main id="content" role="main" class="col-xs-12 col-sm-8">
					<h1>[[*pagetitle]]</h1>
					[[*content]]
				</main>
				<aside class="col-xs-12 col-sm-4">
					[[!CommonTools? &cmd=`loadChunk` &name=`articleSidebar`]] 
				</aside>
			</div><!-- /.row -->
		</div><!-- /container -->
	</section>    
	
	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	    
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]	
	
</body>
</html>