[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>


<div><section class="bannerImage container-fluid noPadding" style="background-image:url([[pthumb? &input=`[[*bannerImage]]` &options=`q=75` &useResizer=`0`]]);">
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]

        <div class="valign">
            <div class="vac">
<div class="container noPadding">
        <div class="col-xs-12 noPadding" id="development[[+idx]]">
          
          <div class="info col-xs-12 col-sm-5">
 
            <hr />
              <h1>[[*pagetitle]]</h1>
              [[*introtext:notempty=`<h3>[[*introtext]]</h3>`]]
            </div>
          </div></div>
          
        </div>
 </div> 
        
</section></div>




<section class="content">
	<div class="container">
        
  [[!CommonTools? &cmd=`loadChunk` &name=`breadcrumbs`]]

		<div>
        <hr />
    	
        </div>
  
  
		<div class="row">
			<main id="content" role="main" class="col-xs-12">
				[[*content]]
			</main>
		</div><!--/.row -->
  
        
	</div>
</section>



<section class="grey">
<div class="container">
<div class="col-xs-12 col-sm-3">

</div>
[[*quickLinkTitle:ne=``:then=`<div class="col-xs-12 col-sm-9 blogHeading">
<h2>[[*quickLinkTitle]]</h2>
</div>`]]

	[[!getImageList?
          &tvname=`quickLink`
          &tpl=`quickLink_tpl`
          &tplOdd=`quickLink_tpl_odd`
	]] 


</div>
</section>


	
 [[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	    
   
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>