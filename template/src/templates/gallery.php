[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
    
    <link rel="stylesheet" type="text/css" href="template/css/jquery.fancybox.css" media="screen" />
	<script type="text/javascript" src="template/js/jquery.fancybox.js" ></script>
    
	<script type="text/javascript">
	$(document).ready(function() {
		$('.fancybox').fancybox({
			'padding':5
		});

		$('.fancybox-media')
		.attr('rel', 'media-gallery')
		.fancybox({
			openEffect : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',
			'width':1000,
			arrows : false,
			helpers : {
				media : {},
				buttons : {}
			}
		});
	});
	</script>
</head>

<body class="showMenu">
	[[!CommonTools? &cmd=`loadChunk` &name=`header`]]

	<div class="galleryContainer clearfix">

 	[[!Gallery? 
	 	&album=`3`
	 	&thumbWidth=`200` 
	 	&thumbHeight=`200` 
	 	&linkToImage=`1` 
	 	&thumbTpl=`galleryThumb_tpl` 
	 	&imageWidth=`1200` 
	 	&imageHeight=`900`  
	 	&imageZoomCrop=`1`
	 	&renderNavControls=`0`
	 	&renderSSControls=`0`
	 	&sort=`createdon` &dir=`DESC`
	 	&numThumbs=`170`
 	]] 
 	
  </div><!--/.galleryContainer -->
    
 	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	    
    [[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]

</body>
</html>
