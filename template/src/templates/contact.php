[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]

	<script type="text/javascript">
	function initialize() {
		var locations = [
		[[!getResources? 
		&parents=`32`
		&tplPath=`<?php echo MODX_WALKERTEMPLATE_PATH; ?>src/chunks/`
		&tpl=`@INLINE  ['[[+pagetitle]]', [[+tv.mapAddress]], 4],` 
		&showHidden=`1`
		&includeContent=`1`
		&includeTVs=`1`
		&includeTVList=`mapAddress`
		&sortdir=`ASC`
		]]
		];

		window.map = new google.maps.Map(document.getElementById('map'), {
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var infowindow = new google.maps.InfoWindow();
		var bounds = new google.maps.LatLngBounds();

		var image = new google.maps.MarkerImage('template/images/map-marker.png', null, null,
			new google.maps.Point(44, 99),
			new google.maps.Size(70, 85)
			);


		for (i = 0; i < locations.length; i++) {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			//		icon: image,
			map: map
		});

			bounds.extend(marker.position);

			google.maps.event.addListener(marker, 'click', (function (marker, i) {
				return function () {
					infowindow.setContent(locations[i][0]);
					infowindow.open(map, marker);
					map.setCenter(marker.getPosition());
				}
			})(marker, i));
		}

		map.fitBounds(bounds);
		map.getBoundsZoomLevel(bounds);

		var listener = google.maps.event.addListener(map, "idle", function () {
			map.setZoom(5);
			google.maps.event.removeListener(listener);
		});
	}

	function loadScript() {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 'callback=initialize';
		document.body.appendChild(script);
	}

	window.onload = loadScript;

	</script>

</head>
<body>

	<div>
		<section class="bannerImage container-fluid noPadding" style="background-image:url([[pthumb? &input=`[[*bannerImage]]` &options=`q=75` &useResizer=`0`]]);">
			[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
			<div class="valign">
				<div class="vac">

					<div class="container noPadding">
						<div class="col-xs-12 noPadding" id="development[[+idx]]">
							<div class="info col-xs-12 col-sm-5">
								<hr />
								<h1>[[*pagetitle]]</h1>
								<h3>[[*introtext]]</h3>
							</div>
						</div>
					</div>

				</div>
			</div> 
		</section>
	</div>

	<section class="content">
		<div class="container">
			[[!CommonTools? &cmd=`loadChunk` &name=`breadcrumbs`]]
			<div><hr /></div>

				<div class="row">
					<main id="content" role="main" class="col-xs-12 col-sm-7">
						[[*content]]
						[[!CommonTools? &cmd=`loadChunk` &name=`contentExtra`]]
					</main>
					<aside class="col-xs-12 col-sm-5">
						[[*contentImage:notempty=`<img src="[[*contentImage:phpthumbof=`w=535`]]" class="img-responsive" alt="[[*pagetitle]] image"/>`]]
					</aside>
				</div><!--/.row -->

				<div class="row">
					<div id="map" class="col-xs-12 col-sm-6 col-lg-6 col-lg-offset-0 col-sm-push-6 col-lg-push-6" style="height: 500px;"></div>
					<aside class="col-xs-12 col-lg-offset-0 col-lg-6 col-sm-6 col-sm-pull-6	col-lg-pull-6 mainContent">
						[[!getResources? 
						&parents=`32`
						&tplPath=`<?php echo MODX_WALKERTEMPLATE_PATH; ?>src/chunks/`
						&tpl=`@INLINE <div class="tabs"><h2 onClick="map.panTo(new google.maps.LatLng([[+tv.mapAddress]])); map.setZoom(16);" class="accordion_button">[[+pagetitle]]<span class="fa fa-chevron-down"></span></h2><div class="accordion_content">[[+content]]</div></div>` 
						&showHidden=`1`
						&includeContent=`1`
						&includeTVs=`1`
						&includeTVList=`mapAddress`
						&sortdir=`ASC`
						]]
					</aside>
				</div>    
			</div>
		</section>

		[[!CommonTools? &cmd=`loadChunk` &name=`footer`]] 
		[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
	</body>
	</html>