[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	<link rel="stylesheet" type="text/css" href="template/css/slick.css">
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
	<script src="/template/js/slick.min.js"></script>
</head>
<body>
  [[!getImageList?
  &tvname=`homeHero`
  &tpl=`homeHero_tpl`
  &limit=`1`
  ]] 
	<section class="homeSlider grey" id="homeSlider">

		<div class="container single-item">
			[[!getImageList?
			&tvname=`homeSlider`
			&tpl=`homeSlider_tpl`
			&limit=`3`
			]] 
		</div><!--/.container -->

	</section><!--/.homeSlider -->

	<section class="homeFeature">

		<div class="container noPadding">

			<div class="col-xs-12 col-sm-6 info">
				<hr />
				<h1>[[*featureHeading1]]</h1>
				<h2>[[*featureHeading2]]</h2>
				<p>[[*featureText]]</p>
				<a class="btn" href="[[*featureLink]]">Discover More</a>
			</div><!--/.col -->

			<div class="col-xs-12 col-sm-6">
				<img src="[[*featureImage]]" class="img-responsive" alt="[[*featureHeading2]]"/>
			</div><!--/.col -->

		</div><!--/.container -->
		
	</section><!--/.homeFeature -->

	<section class="latestArticles grey">

		<div class="container">
			
			<div class="col-xs-12 col-sm-3"></div>
			<div class="col-xs-12 col-sm-9 blogHeading">
	  			<h2>Media &amp; Speeches</h2>
	  			<a href="[[~12]]" class="btn">View All</a>
  			</div><!--/.col -->
		
		</div><!--/.container -->

		<div class="container noPadding">
			[[!getResources?
			&parents=`12`
			&limit=`1`
			&showHidden=`1`
			&sortdir=`DESC`
			&sortby=`publishedon`

			&includeTVs=`1`
			&includeTVList=`articleImage,zoomCrop`
			&tvFilters=`isSticky==Yes`

			&tplPath=`<?php echo MODX_WALKERTEMPLATE_PATH; ?>src/chunks/`
			&tpl=`@FILE getResources_latestNewsOdd.html`
			]]

			[[!getResources?
			&parents=`12`
			&limit=`3`
			&showHidden=`1`
			&sortdir=`DESC`
			&sortby=`publishedon`
			&includeTVs=`1`
			&includeTVList=`articleImage,zoomCrop`
			&tplPath=`<?php echo MODX_WALKERTEMPLATE_PATH; ?>src/chunks/`
			&tpl=`@FILE getResources_latestNewsOdd.html`
			&tplOdd=`@FILE getResources_latestNews.html`
			]]
		</div><!--/.container -->

	</section>

[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	    
<script type="text/javascript">
$(document).ready(function(){
  $('.single-item').slick({
	dots: true,
	arrows: false
	});
});
</script>

[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>