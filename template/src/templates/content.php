[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>

	<div>
		<section class="bannerImage container-fluid noPadding" style="background-image:url(
		[[*bannerImage:neq=``:then=`[[pthumb? &input=`[[*bannerImage]]` &options=`w=1980` &useResizer=`0`]]`:else=`[[pthumb? &input=`[[CommonTools? &cmd=`getTemplateVar` &tvName=`bannerImage` &pageID=`[[UltimateParent? &topLevel=`1`]]`]]` &options=`w=1980` &useResizer=`0`]]`]]
		);">
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]

			<div class="valign">
				<div class="vac">
					<div class="container noPadding">
						<div class="col-xs-12 noPadding" id="development[[+idx]]">
							<div class="info col-xs-12 col-sm-5">
								<hr />
								<h1>[[*pagetitle]]</h1>
								[[*introtext:notempty=`<h3>[[*introtext]]</h3>`]]
							</div><!--/.info -->
						</div><!--/.col -->
					</div><!--/.container -->
				</div><!--/.vac -->
			</div><!--/.valign --> 
		</section><!--/.bannerImager -->
	</div>

	<section class="content">
		<div class="container">
			[[!CommonTools? &cmd=`loadChunk` &name=`breadcrumbs`]]
			<div><hr /></div>
			<div class="row">
				<main id="content" role="main" class="col-xs-12 col-sm-7">
					[[*content]]
				</main>
				<aside class="col-xs-12 col-sm-5">
					[[*contentImage:notempty=`<img src="[[*contentImage:phpthumbof=`w=535`]]" class="img-responsive" alt="[[*pagetitle]] image"/>`]]
				</aside>
			</div><!--/.row -->
		</div><!--/.container -->
	</section>

	<section class="grey">
		<div class="container">
			<div class="col-xs-12 col-sm-3"></div><!--/.col -->
			[[*quickLinkTitle:ne=``:then=`<div class="col-xs-12 col-sm-9 blogHeading"><h2>[[*quickLinkTitle]]</h2></div>`]]

			[[!getImageList?
			&tvname=`quickLink`
			&tpl=`quickLink_tpl`
			&tplOdd=`quickLink_tpl_odd`
			]] 
		</div><!--/.container -->
	</section>

	[[!CommonTools? &cmd=`loadChunk` &name=`footer`]]	    
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]
</body>
</html>