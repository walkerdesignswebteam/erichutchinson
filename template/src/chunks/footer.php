<section class="social grey">
	<div class="container clearfix">

		<div class="col-xs-12 col-sm-3 socialMedia">
			<a class="facebook" href="https://www.facebook.com/erichutchinsonmp" target="_blank"><i class="fa fa-facebook"></i><span class="visuallyhidden">Facebook</span></a>
			<a class="twitter" href="https://twitter.com/hutchinson_eric" target="_blank"><i class="fa fa-twitter"></i><span class="visuallyhidden">Twitter</span></a>
			<a class="youtube"><i class="fa fa-youtube"></i><span class="visuallyhidden">youtube</span></a>
		</div>

		<div class="col-xs-12 col-sm-3">
			<h3>Feedback</h3>
			<p>Let Eric know what good things you are achieving in your town.</p>
			<a class="btn" href="mailto:eric.hutchinson.mp@aph.gov.au">Email Eric</a>
		</div>

		<div class="col-xs-12 col-sm-6 noPadding">

			<div class="col-xs-12">
				<h3>SIGNUP FOR ENEWS</h3>
			</div>

			<div class="col-xs-12 col-sm-6">
				<p>Keep up to date with the latest electorate information by signing up to Eric's newsletter.</p>
			</div>

			<div class="col-xs-12 col-sm-6">
				<!-- Begin MailChimp Signup Form -->
				<div id="mc_embed_signup" class="hideLabel">
					<form class="newsletter-form fl" action="http://us3.list-manage.com/subscribe/post?u=c48fb51a6dbd385ef97dc9437&amp;id=8297f77afb" method="post" target="popupwindow" onsubmit="window.open('http://us3.list-manage.com/subscribe/post?u=c48fb51a6dbd385ef97dc9437&amp;id=8297f77afb', 'popupwindow', 'scrollbars=yes,width=650,height=520');return true">
						<label>Email</label>
						<input type="text" name="EMAIL" class="required email" placeholder="Your Email" value="" id="mce-EMAIL">
						<br/>
						<button type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="btn submit button">Submit</button>
					</form>
				</div>
				<!--End mc_embed_signup-->
			</div>
		</div>

		<div class="col-xs-12 col-sm-3"></div>
	</div>
</section>

<footer role="contentinfo" class="container">
	<div class="row">
		<div class="col-xs-4  noMargin noPadding">
			<div class="logoWrap">
				<div class="logo">
					<a href="[[++site_url]]" title="Eric Hutchinson">
						<img src= "template/images/eric-hutchinson-footer.jpg" alt="Eric Hutchinson" class="img-responsive" />
					</a>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-8">
			<nav>
				[[Wayfinder? 
				&startId=`0` 
				&level=`1`
				&excludeDocs=`1`
				&lastClass=`last`
				]]
			</nav>
			<p class="disclaimer">
				<span>&copy; Copyright erichuhinson.com.au 2015</span>
				<span><a href="[[~4]]">Terms & Conditions</a></span>
				<span><a href="[[~5]]">Privacy Policy</a></span>
				<span>Website by <a href="http://walkerdesigns.com.au/web/" rel="external" title="Website design, development, hosting and copywriting by Walker Designs, Launceston, Hobart, Tasmania, Australia">Walker Designs</a></span>

			</p>
		</div>
	</div>
</footer>