<link rel="shortcut icon" type="image/ico" href="/favicon.ico">
<link rel="apple-touch-icon" href="template/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="template/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="template/images/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="144x144" href="template/images/apple-touch-icon-144x144.png">

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<link href='http://fonts.googleapis.com/css?family=Raleway:400,600,700%7COpen+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

[[!MinifyX?
&minifyCss=`0`
&minifyJs=`0`

&cssSources=`
/template/css/styles.css,
/template/css/jquery-ui.min.css
`

&jsSources=`
/template/bs-assets/javascripts/bootstrap.js,
/template/bs-assets/javascripts/rwd-table.min.js,
/template/js/jquery.validate.min.js,
/template/js/jquery-ui.min.js,
/template/js/jquery.mmenu.min.all.js,
/template/js/template.js
`
]]

[[+MinifyX.css]]

