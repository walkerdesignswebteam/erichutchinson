<nav id="mobile-nav" class="mobileNav">
	[[Wayfinder? 
	&startId=`0` 
	&level=`3`
	]]
</nav>	
<div>
	<header id="header" class="header">

		<div class="container-fluid noPadding">

			<div class="row noPadding noMargin headerTop">
				<div id="mobile-menu-butt" class="mobilebars hidden-lg hidden-md"><i class="fa fa-bars"></i></div>

				<div class="col-lg-3 col-md-3 col-sm-5 col-sm-offset-0 col-xs-8 col-xs-offset-2 noMargin noPadding">
					<div class="logoWrap">
						<div class="logo">
							<a href="[[++site_url]]" title="Eric Hutchinson">
								<img src= "template/images/eric-hutchinson-logo.png" alt="Eric Hutchinson" class="img-responsive" />
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-9 col-sm-6 col-xs-12">
					<nav class="navMain hidden-xs hidden-sm">
						[[Wayfinder? 
						&startId=`0` 
						&excludeDocs=`1`
						&level=`2`
						]]
					</nav>
				</div>

			</div>
		</div>
	</header>
</div>